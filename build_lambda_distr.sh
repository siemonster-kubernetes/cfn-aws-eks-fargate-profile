#!/usr/bin/env bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"
docker run --rm -it -v "$(pwd)/build:/build:ro" -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:python3.7
