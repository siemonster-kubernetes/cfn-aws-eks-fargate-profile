#!/usr/bin/env bash

set -e

# /var/task

yum install -y zip

pip3 install -r /src/requirements.txt -t .
cp /src/*.py .

GLOBIGNORE=.:..; zip -9 -r /out/lambda * ; unset GLOBIGNORE
