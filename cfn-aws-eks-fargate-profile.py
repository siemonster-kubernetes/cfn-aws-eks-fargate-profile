#!/usr/bin/env python
from crhelper import CfnResource
from botocore.exceptions import ClientError
import boto3
import logging


logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = boto3.client('eks')
except Exception as e:
    helper.init_failure(e)


def get_properties(event):
    allowed_params = ['fargateProfileName', 'clusterName', 'podExecutionRoleArn', 'subnets', 'selectors', 'tags']
    return {k: v for k, v in event['ResourceProperties'].items() if k in allowed_params}


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment
    try:
        properties = get_properties(event)
        response = client.create_fargate_profile(**properties)
        logger.info("Started EKS Fargate profile creation: {}".format(response['fargateProfile']['fargateProfileArn']))

        helper.Data.update({
            'Arn': response['fargateProfile']['fargateProfileArn'],
            'Name': response['fargateProfile']['fargateProfileName'],
        })
        return response['fargateProfile']['fargateProfileArn']
    except Exception as e:
        helper.init_failure(e)


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes


@helper.delete
def delete(event, context):
    logger.info("Got Delete")
    properties = get_properties(event)
    response = client.delete_fargate_profile(
        clusterName=properties['clusterName'],
        fargateProfileName=properties['fargateProfileName'],
    )

    logger.info("EKS Fargate profile destroy initiated: {}".format(response['fargateProfile']['fargateProfileArn']))


@helper.poll_create
def poll_create(event, context):
    logger.info("Got create poll")
    # Return a resource id or True to indicate that creation is complete. if True is returned an id will be generated
    properties = get_properties(event)
    try:
        response = client.describe_fargate_profile(
            clusterName=properties['clusterName'],
            fargateProfileName=properties['fargateProfileName'],
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            helper.init_failure("Status: FAILED")

    logger.info(response)

    if response['fargateProfile']['status'] == 'ACTIVE':
        return True

    if  response['fargateProfile']['status'] == 'FAILED':
        helper.init_failure("Status: FAILED")


@helper.poll_delete
def poll_delete(event, context):
    logger.info("Got delete poll")

    properties = get_properties(event)
    try:
        response = client.describe_fargate_profile(
            clusterName=properties['clusterName'],
            fargateProfileName=properties['fargateProfileName'],
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            return True

    logger.info(response)


def handler(event, context):
    helper(event, context)
